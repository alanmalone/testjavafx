package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Person;

//���������� ���������� �� �������������� ������

public class PersonEditDialogController {
	
	@FXML
	private TextField firstNameField;
	
	@FXML
	private TextField lastNameField;
	
	@FXML
	private TextField cityField;
	
	@FXML
	private TextField streetField;
	
	@FXML
	private TextField postalCodeField;
	
	private Stage primaryStage;
	private Person person;
	private boolean okClicked = false;
	
	@FXML
	private void initialize() {
		
	}
	
	//��������� �����, �� ������� ����������� ����������
	public void setDialogStage(Stage dialogStage) {
		this.primaryStage = dialogStage;
		this.primaryStage.getIcons().add(new Image("file:resources/images/database.png"));
	}
	
	//�������������� ���������� ������
	public void setPerson(Person person) {
		this.person = person;
		
		firstNameField.setText(person.getFirstName());
        lastNameField.setText(person.getLastName());
        streetField.setText(person.getStreet());
        postalCodeField.setText(Integer.toString(person.getPostalCode()));
        cityField.setText(person.getCity());
	}
	
	//�������� ������� �� ��
	public boolean isOkClicked() {
		return okClicked;
	}
	
	//��������� ��������� � ��������� �����
	public void handleOk() {
		if (isInputValid()) {
			person.setFirstName(firstNameField.getText());
            person.setLastName(lastNameField.getText());
            person.setStreet(streetField.getText());
            person.setPostalCode(Integer.parseInt(postalCodeField.getText()));
            person.setCity(cityField.getText());
            
            okClicked = true;
            primaryStage.close();
		}
	}
	
	//������� �� ������ ������ � �������� �����
	@FXML
    private void handleCancel() {
        primaryStage.close();
    }

	//�������� �������� ��������
	private boolean isInputValid() {
		String errorMessage = "";

        if (firstNameField.getText() == null || firstNameField.getText().length() == 0) {
            errorMessage += "�� ������� ���!\n"; 
        }
        if (lastNameField.getText() == null || lastNameField.getText().length() == 0) {
            errorMessage += "�� ������� �������!\n"; 
        }
        if (streetField.getText() == null || streetField.getText().length() == 0) {
            errorMessage += "�� ������� �����!\n"; 
        }

        if (postalCodeField.getText() == null || postalCodeField.getText().length() == 0) {
            errorMessage += "�� ������ �������� ������!\n"; 
        } else {
            // try to parse the postal code into an int.
            try {
                Integer.parseInt(postalCodeField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "���� ��� - ����� ��������!\n"; 
            }
        }

        if (cityField.getText() == null || cityField.getText().length() == 0) {
            errorMessage += "����� �� ������!\n"; 
        }
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(primaryStage);
            alert.setTitle("������");
            alert.setHeaderText("������� ���������� ��������");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
	}
}
