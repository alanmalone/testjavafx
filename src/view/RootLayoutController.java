package view;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import main.MainApp;

//���������� �������� ����
public class RootLayoutController {
	private MainApp mainApp;
	
	public void setMailApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
	
	//��������� ������ �����
	@FXML
	private void handleNew() {
		mainApp.getPersonData().clear();
		mainApp.setPersonFilePath(null);
	}
	
	//��������� ������ �������
	@FXML
	private void handleOpen() {
		FileChooser fc = new FileChooser(); //FileCooser ������ ��� ������� ��������� ����
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"); //������ ������ �� ������ XML-�����
		fc.getExtensionFilters().add(extFilter); //��������� ������
		
		File file = fc.showOpenDialog(mainApp.getPrimaryStage()); //��������� ������ ��� �������� �����
		if (file != null) {
			mainApp.loadPersonDataFromFile(file);
		}
	}
	
	
	//��������� ������ ���������
	@FXML
	private void handleSave() {
		File personFile = mainApp.getPersonFilePath();
		if (personFile != null) {
			mainApp.savePersonDataToFile(personFile);
		} else {
			handleSaveAs();
		}
	}

	//��������� ������ ��������� ���
	@FXML
	private void handleSaveAs() {
		// TODO Auto-generated method stub
		FileChooser fc = new FileChooser();
		
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", ".xml");
		fc.getExtensionFilters().add(extFilter);
		
		File file = fc.showSaveDialog(mainApp.getPrimaryStage());  //��������� ������ ��� �������� �����
		
		if (file != null) {
			if (!file.getPath().endsWith(".xml")) {
				file = new File(file.getPath()+".xml");
			}
			mainApp.savePersonDataToFile(file);
		}
	}
	
	//��������� ������ ������������� � HTML
	@FXML
	private void handleTransformToHTML() {
		
		File file = mainApp.getPersonFilePath();
		
		if (file != null) {
			FileChooser fc = new FileChooser();
			
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("HTML files (*.html)", ".html");
			fc.setSelectedExtensionFilter(extFilter);
			
			File fcFile = fc.showSaveDialog(mainApp.getPrimaryStage());
			
			if (fcFile != null) {
				if (!file.getPath().endsWith(".html")) {
					fcFile = new File(fcFile.getPath() + ".html");
				}
				mainApp.saveHTMLFile(fcFile, file);
			}
		} else {
			 Alert alert = new Alert(AlertType.ERROR);
	            alert.initOwner(mainApp.getPrimaryStage());
	            alert.setTitle("�� ������� �����");
	            alert.setHeaderText("�������� ����");
	            alert.setContentText("�������� ���� ��� ��������������");

	            alert.showAndWait();
		}
	}
	
	//��������� ������ �����
	@FXML
	private void handleExit() {
		System.exit(0);
	}
}
