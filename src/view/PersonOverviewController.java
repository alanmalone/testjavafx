package view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import main.MainApp;
import model.Person;

//���������� ����������� ���� �������
public class PersonOverviewController {
	@FXML
	private TableView<Person> personTable;
	@FXML
	private TableColumn<Person, String> firstNameColumn;
	@FXML
	private TableColumn<Person, String> lastNameColumn;
	
	@FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label streetLabel;
    @FXML
    private Label postalCodeLabel;
    @FXML
    private Label cityLabel;
    @FXML
    private Label birthdayLabel;
    
	private MainApp mainApp;
    
    public PersonOverviewController() {
    	
    }
    
    @FXML
    private void initialize() {
    	firstNameColumn.setCellValueFactory(cellData->cellData.getValue().firstNameProperty()); //����� �������� �� FXML-�� � ������ � firstNameColumn
    	lastNameColumn.setCellValueFactory(cellData->cellData.getValue().lastNameProperty());
    	
    	showPersonDetails(null);
    	
    	personTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showPersonDetails(newValue));
    }
    
    public void setMainApp(MainApp mainApp) {
    	this.mainApp = mainApp;
    	personTable.setItems(mainApp.getPersonData());
    }
    
    //����������� ������� � ������
    public void showPersonDetails(Person person) {
    	if (person != null) {
    		firstNameLabel.setText(person.getFirstName());
    		lastNameLabel.setText(person.getLastName());
    		streetLabel.setText(person.getStreet());
    		postalCodeLabel.setText(String.valueOf(person.getPostalCode()));
    		cityLabel.setText(person.getCity());
    	} else {
    		firstNameLabel.setText("");
    		lastNameLabel.setText("");
    		streetLabel.setText("");
    		postalCodeLabel.setText("");
    		cityLabel.setText("");
    	}
    }
    
    //��������� ������ �������
    @FXML
    private void handleDeletePerson() {
    	int selectIndex = personTable.getSelectionModel().getSelectedIndex();
    	if (selectIndex >= 0) {
    		personTable.getItems().remove(selectIndex);
    	} else {
    		Alert alert = new Alert(AlertType.WARNING);
    		alert.initOwner(mainApp.getPrimaryStage());
    		alert.setTitle("��� �����������");
    		alert.setHeaderText("�� �������� �� ���� ������");
    		alert.setContentText("�������� ������");
    		
    		alert.showAndWait();
    	}
    }
    
    //��������� ������ �������������
    @FXML
    private void handleEdit() {
    	Person selectedPerson = personTable.getSelectionModel().getSelectedItem();
    	if (selectedPerson != null) {
    		boolean isOkClicked = mainApp.showPersonEditDialog(selectedPerson);
    		if (isOkClicked) {
    			showPersonDetails(selectedPerson);
    		}
    	} else {
    		Alert alert = new Alert(AlertType.WARNING);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("������ �� �������");
            alert.setHeaderText("�� ���� ������ �� ���������");
            alert.setContentText("�������� ������ � �������");

            alert.showAndWait();
    	}
    }
    
    //��������� ����� �����
    @FXML
    private void handleNew() {
    	Person tempPerson = new Person();
    	boolean okClicked = mainApp.showPersonEditDialog(tempPerson);
    	if (okClicked) {
    		mainApp.getPersonData().add(tempPerson);
    	}
    }
}
