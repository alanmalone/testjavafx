package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import model.*;
import view.PersonEditDialogController;
import view.PersonOverviewController;
import view.RootLayoutController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage; //������� �����
	private BorderPane rootLayout; //�������� ����������� ����
	private ObservableList<Person> personData = FXCollections.observableArrayList(); //���� ������ ������ ������
	
	public MainApp() {

    }
	
	//��������� ����� ������
	public ObservableList<Person> getPersonData() {
        return personData;
    }
	
	//����� ������� �����������, ��� ����������� ����������
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("�������� �����");
		
		this.primaryStage.getIcons().add(new Image("file:resources/images/database.png")); //���������� ������ ��� ������ ����������
		
		initRootLayout(); //������������� ��������� ����
		showPersonOverview(); //����������� ������
	}

	//����������� ������
	private void showPersonOverview() {
		// TODO Auto-generated method stub
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/PersonOverview.fxml")); //������� ������ ����� ���� ������
			AnchorPane personOverview = (AnchorPane) loader.load(); //� ������ ������� AnchrPane �� ����� ������
			rootLayout.setCenter(personOverview); //���������� ������ ������ ��������� ���� ������
			
			PersonOverviewController controller = loader.getController(); //��������� ���������
			controller.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//������������� ��������� ����
	private void initRootLayout() {
		// TODO Auto-generated method stub
		try {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("../view/RootLayout.fxml")); //������� ������ ����� ���� ������
		rootLayout = (BorderPane) loader.load(); //��������� ������� BorderPane �� ������ � ����������� ��� ��� �������� ����
		
		Scene scene = new Scene(rootLayout);	//��������� �� �����
		primaryStage.setScene(scene);			//�������� ����
		
		RootLayoutController controller = loader.getController(); //��������� ����������
		controller.setMailApp(this);
		primaryStage.show(); //���������� �����
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//���� ������������ �������� ������ � XML, �� ��������� ��
		File file = getPersonFilePath();
	    if (file != null) {
	        loadPersonDataFromFile(file); //��������� ������ �� XML
	    }
	}
	
	
	//���������� ������� �����
	public Stage getPrimaryStage() {
        return primaryStage;
    }

	public static void main(String[] args) {
		launch(args);
	}
	
	
	//�������� ���� � ��������������� ������
	public boolean showPersonEditDialog(Person person) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("../view/PersonEditDialog.fxml")); //������� ������ ����� ������
			AnchorPane page = (AnchorPane) loader.load(); //��������� AnchorPane �� ������
			
			//��������� ����
			Stage dialogStage = new Stage(); 
			dialogStage.setTitle("�������������� ������"); //���� ��������
			dialogStage.initModality(Modality.WINDOW_MODAL); //���� ��� ����
			dialogStage.initOwner(primaryStage); //��� ������������
			Scene scene = new Scene(page); //���������� �������� ������� ����� ����
			dialogStage.setScene(scene); //����������� ���� �����
			
			PersonEditDialogController controller = loader.getController(); //���������� ����������
			controller.setDialogStage(dialogStage);
			controller.setPerson(person);
			
			dialogStage.showAndWait(); //���������� ����
			
			return controller.isOkClicked();  //���������� �������� ����� �� ����
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	//�������� ���� � ��������
	public File getPersonFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class); //����� �� �������� ����
		String filePath = prefs.get("filePath", null); //�� �����
		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}
	
	//��������� ����, ��� �������� ������
	public void setPersonFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
		if (file != null) {
			prefs.put("filePath", file.getPath());
			primaryStage.setTitle("�������� ����� " + file.getName());
		} else {
			prefs.remove("filePath");
			primaryStage.setTitle("�������� �����");
		}
	}
	
	//��������� ������ �� XML-��
	public void loadPersonDataFromFile(File file) {
		try {
			JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
			Unmarshaller um = context.createUnmarshaller(); //����� Unmarshaller ������ ��� ������������ XML-��
			
			/*
			 * ��� ��� � ��� personData ���������� ���� ObservableList �� �� ����� � ���
			 * ��������� ��������� @XmlRootElement, ������� �� ������ ������� �����-������� PersonListWrapper*/
			
			PersonListWrapper wrapper = (PersonListWrapper) um.unmarshal(file);
			
			personData.clear(); //������� ��� ����
		    personData.addAll(wrapper.getPersons()); //� ��������� � ���� ������

			setPersonFilePath(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//��������� ������ � XML-��
	public void savePersonDataToFile(File file) {
		try {
			JAXBContext context = JAXBContext.newInstance(PersonListWrapper.class);
			Marshaller m = context.createMarshaller(); //����� Marshaller ������ ��� ������������ XML-��
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			PersonListWrapper wrapper = new PersonListWrapper();
			wrapper.setPersons(personData);
			
			m.marshal(wrapper, file);
			
			setPersonFilePath(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//����� ��� �������������� XML-�� � HTML-��
	public void saveHTMLFile(File saveToFile, File mainXMLFile) {
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			
			Transformer transformer = tFactory.newTransformer(
					new javax.xml.transform.stream.StreamSource("file:resources/xsl/person.xsl"));
			transformer.transform(new javax.xml.transform.stream.StreamSource
		            (mainXMLFile.getPath().toString()), new javax.xml.transform.stream.StreamResult
		            ( new FileOutputStream(saveToFile.getPath().toString())));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
