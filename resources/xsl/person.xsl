<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
version="1.0">
<xsl:template match="/">
<html>
	<head>
		<meta charset="utf-8"></meta>
    	<meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
    	<meta name="viewport" content="width=device-width, initial-scale=1"></meta>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<title>Персональные данные</title>
	</head>
	<body>
		<div class="container">
			<table class="table table-bordered">
				<tr>
					<th>Имя</th>
					<th>Фамилия</th>
					<th>Город</th>
					<th>Улица</th>
					<th>Почтовый индекс</th>
				</tr>
			<xsl:for-each select="persons/person">
				<tr>
					<td><xsl:value-of select="firstName"/></td>
					<td><xsl:value-of select="lastName"/></td>
					<td><xsl:value-of select="city"/></td>
					<td><xsl:value-of select="street"/></td>
					<td><xsl:value-of select="postalCode"/></td>
				</tr>
		</xsl:for-each>
		</table>
		</div>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>